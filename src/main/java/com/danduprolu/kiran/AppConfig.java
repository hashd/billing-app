package com.danduprolu.kiran;

import com.danduprolu.kiran.billstop.models.CustomerType;
import com.danduprolu.kiran.billstop.models.Item;
import com.danduprolu.kiran.billstop.models.ShoppingCart;
import com.danduprolu.kiran.billstop.processors.ChristmasDiscountProcessor;
import com.danduprolu.kiran.billstop.processors.DiscountProcessor;
import com.danduprolu.kiran.billstop.services.BillingService;
import com.danduprolu.kiran.billstop.services.SimpleBillingService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedList;
import java.util.List;

@Configuration
public class AppConfig {
    @Bean
    public BillingService billingService() {
        DiscountProcessor lowRegular = new ChristmasDiscountProcessor("XMAX_REGULAR_LOW", CustomerType.REGULAR, 0, 500000, 0);
        DiscountProcessor medRegular = new ChristmasDiscountProcessor("XMAX_REGULAR_MED", CustomerType.REGULAR, 500000, 1000000, 10);
        DiscountProcessor highRegular = new ChristmasDiscountProcessor("XMAX_REGULAR_HIGH", CustomerType.REGULAR, 1000000, Long.MAX_VALUE, 20);
        DiscountProcessor lowPremium = new ChristmasDiscountProcessor("XMAX_PREMIUM_LOW", CustomerType.PREMIUM, 0, 500000, 10);
        DiscountProcessor medPremium = new ChristmasDiscountProcessor("XMAX_PREMIUM_MED", CustomerType.PREMIUM, 500000, 1000000, 20);
        DiscountProcessor highPremium = new ChristmasDiscountProcessor("XMAX_PREMIUM_HIGH", CustomerType.PREMIUM, 1000000, Long.MAX_VALUE, 30);

        List<DiscountProcessor> discountsList = new LinkedList<>();
        discountsList.add(lowRegular);
        discountsList.add(medRegular);
        discountsList.add(highRegular);
        discountsList.add(lowPremium);
        discountsList.add(medPremium);
        discountsList.add(highPremium);

        return new SimpleBillingService(discountsList);
    }

    @Bean
    public App app() {
        return new App();
    }

    @Bean
    public ShoppingCart sampleShoppingCart() {
        Item almonds250 = new Item("ALMONDS_250", "Almonds 250 gms", 50000);
        Item almonds500 = new Item("ALMONDS_500", "Almonds 500 gms", 80000);
        Item cashew250 = new Item("CASHEW_250", "Cashew 250 gms", 80000);
        Item cashew500 = new Item("CASHEW_500", "Cashew 500 gms", 150000);
        Item pista250 = new Item("PISTA_250", "Pista 250 gms", 100000);
        Item pista500 = new Item("PISTA_500", "Pista 500 gms", 180000);

        ShoppingCart.ShoppingCartBuilder cartBuilder = new ShoppingCart.ShoppingCartBuilder();
        cartBuilder.add(almonds250, 2);
        cartBuilder.add(cashew500, 4);
        cartBuilder.add(pista250, 8);

        return cartBuilder.build();
    }
}
