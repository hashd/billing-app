package com.danduprolu.kiran.billstop.processors;

import com.danduprolu.kiran.billstop.models.Customer;
import com.danduprolu.kiran.billstop.models.CustomerType;
import com.danduprolu.kiran.billstop.models.Discount;
import com.danduprolu.kiran.billstop.models.ShoppingCart;

public class ChristmasDiscountProcessor implements DiscountProcessor {
    private String       discountCode;
    private CustomerType customerType;
    private long         minSlab;
    private long         maxSlab;
    private double       percentage;

    public ChristmasDiscountProcessor(String discountCode, CustomerType customerType, long minSlab, long maxSlab, double percentage) {
        this.discountCode = discountCode;
        this.customerType = customerType;
        this.minSlab = minSlab;
        this.maxSlab = maxSlab;
        this.percentage = percentage;
    }

    @Override
    public Discount apply(Customer customer, ShoppingCart cart) {
        long discount = 0;
        if (customer.getType() == customerType) {
            long totalCartCost = cart.getTotalCost();
            if (totalCartCost > minSlab) {
                discount = (long) (((Math.min(totalCartCost, maxSlab) - minSlab) * percentage)/100);
            }
        }

        return new Discount(discountCode, discount);
    }
}
