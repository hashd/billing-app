package com.danduprolu.kiran.billstop.processors;

import com.danduprolu.kiran.billstop.models.Customer;
import com.danduprolu.kiran.billstop.models.Discount;
import com.danduprolu.kiran.billstop.models.ShoppingCart;

@FunctionalInterface
public interface DiscountProcessor {
    Discount apply(Customer customer, ShoppingCart cart);
}
