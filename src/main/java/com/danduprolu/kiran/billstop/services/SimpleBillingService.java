package com.danduprolu.kiran.billstop.services;

import com.danduprolu.kiran.billstop.models.*;
import com.danduprolu.kiran.billstop.processors.DiscountProcessor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SimpleBillingService implements BillingService {
    private List<DiscountProcessor> discounts;

    public SimpleBillingService(List<DiscountProcessor> discounts) {
        this.discounts = discounts;
    }

    @Override
    public Bill createBill(Customer customer, ShoppingCart cart) {
        List<Discount> discountList = discounts.stream()
                .map(dp -> dp.apply(customer, cart))
                .filter(Discount::isNonZero)
                .collect(Collectors.toList());

        return new Bill(customer, cart, discountList);
    }
}
