package com.danduprolu.kiran.billstop.services;

import com.danduprolu.kiran.billstop.models.Bill;
import com.danduprolu.kiran.billstop.models.Customer;
import com.danduprolu.kiran.billstop.models.ShoppingCart;

public interface BillingService {
    Bill createBill(Customer customer, ShoppingCart cart);
}
