package com.danduprolu.kiran.billstop.models;

public class ShoppingEntry implements Cloneable {
    private Item item;
    private int  quantity;

    public ShoppingEntry(Item item, int quantity) {
        this.item = item;
        this.quantity = quantity;
    }

    public Item getItem() {
        return item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void increment() {
        incrementBy(1);
    }

    public void incrementBy(int n) {
        quantity = quantity + n;
    }

    public void decrement() {
        decrementBy(1);
    }

    public void decrementBy(int n) {
        if (quantity >= n) {
            quantity = quantity - n;
        }
    }

    public long getTotalCost() {
        return item.getCost() * quantity;
    }

    public boolean isEmpty() {
        return quantity == 0;
    }

    public ShoppingEntry clone() {
        return new ShoppingEntry(item, quantity);
    }
}
