package com.danduprolu.kiran.billstop.models;

import javafx.util.Builder;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ShoppingCart {
    List<ShoppingEntry> entries;

    public ShoppingCart() {
        this.entries = new LinkedList<>();
    }

    private ShoppingCart(List<ShoppingEntry> entries) {
        this.entries = entries;
    }

    public List<ShoppingEntry> getEntries() {
        return entries.stream().map(ShoppingEntry::clone).collect(Collectors.toList());
    }

    public long getTotalCost() {
        return entries.stream().mapToLong(ShoppingEntry::getTotalCost).sum();
    }

    public static ShoppingCartBuilder toBuilder(ShoppingCart cart) {
        ShoppingCartBuilder cartBuilder = new ShoppingCartBuilder();
        cart.entries.forEach(e -> cartBuilder.add(e.getItem(), e.getQuantity()));

        return cartBuilder;
    }

    public static class ShoppingCartBuilder implements Builder<ShoppingCart> {
        private Map<String, ShoppingEntry> entries;

        public ShoppingCartBuilder() {
            this.entries = new LinkedHashMap<>();
        }

        public ShoppingCartBuilder add(Item item) {
            return this.add(item, 1);
        }

        public ShoppingCartBuilder add(Item item, int quantity) {
            String itemCode = item.getCode();
            if (!entries.containsKey(itemCode)) {
                entries.put(itemCode, new ShoppingEntry(item, 0));
            }
            entries.get(itemCode).incrementBy(quantity);

            return this;
        }

        public ShoppingCartBuilder remove(String itemCode) {
            return remove(itemCode, 1);
        }

        public ShoppingCartBuilder remove(String itemCode, int quantity) {
            if (!entries.containsKey(itemCode)) {
                return this;
            }
            entries.get(itemCode).decrementBy(quantity);
            return this;
        }

        public ShoppingCartBuilder removeAll(String itemCode) {
            return remove(itemCode, entries.get(itemCode).getQuantity());
        }

        @Override
        public ShoppingCart build() {
            List<ShoppingEntry> cartEntries = entries.entrySet().stream()
                    .map(s -> s.getValue().clone()).collect(Collectors.toList());

            return new ShoppingCart(cartEntries);
        }
    }
}
