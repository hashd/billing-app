package com.danduprolu.kiran.billstop.models;

public class Discount {
    private String code;
    private long amount;

    public Discount(String code, long amount) {
        this.code = code;
        this.amount = amount;
    }

    public long getAmount() {
        return amount;
    }

    public boolean isNonZero() {
        return amount != 0;
    }
}
