package com.danduprolu.kiran.billstop.models;

public class Item {
    private String code;
    private String name;
    private long   cost;

    public Item(String code, String name, long cost) {
        this.code = code;
        this.name = name;
        this.cost = cost;
    }

    public long getCost() {
        return cost;
    }

    public String getCode() {
        return code;
    }
}
