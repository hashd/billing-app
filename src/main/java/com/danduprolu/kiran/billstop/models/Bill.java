package com.danduprolu.kiran.billstop.models;

import java.util.List;

public class Bill {
    private Customer       customer;
    private ShoppingCart   cart;
    private List<Discount> discounts;

    public Bill(Customer c, ShoppingCart sc, List<Discount> discountList) {
        customer = c;
        cart = sc;
        discounts = discountList;
    }

    public long getTotalCost() {
        return getCartCost() - getTotalDiscount();
    }

    public long getCartCost() {
        return cart.getEntries().stream().mapToLong(ShoppingEntry::getTotalCost).sum();
    }

    public long getTotalDiscount() {
        return discounts.stream().mapToLong(Discount::getAmount).sum();
    }
}
