package com.danduprolu.kiran.billstop.models;

public class Customer {
    private CustomerType type;

    public Customer(CustomerType type) {
        this.type = type;
    }

    public CustomerType getType() {
        return type;
    }
}
