package com.danduprolu.kiran.billstop.models;

public enum CustomerType {
    REGULAR, PREMIUM
}
