package com.danduprolu.kiran;

import com.danduprolu.kiran.billstop.models.Bill;
import com.danduprolu.kiran.billstop.models.Customer;
import com.danduprolu.kiran.billstop.models.CustomerType;
import com.danduprolu.kiran.billstop.models.ShoppingCart;
import com.danduprolu.kiran.billstop.services.BillingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    @Autowired
    private BillingService billingService;

    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
        App app = (App) ctx.getBean("app");

        ShoppingCart cart = (ShoppingCart) ctx.getBean("sampleShoppingCart");
        Customer customer = new Customer(CustomerType.REGULAR);

        Bill bill = app.billingService.createBill(customer, cart);
        System.out.printf("%d:%d:%d\n", bill.getTotalCost() / 100, bill.getCartCost() / 100, bill.getTotalDiscount() / 100);
        return;
    }
}
